# TEs and DE genes



## Input

- [ ] List of genes that are related to TEs (output of tes-r-analysis), with their counts for different conditions [(example)](https://forgemia.inra.fr/marie-hervine.lefevre/tes-and-de-genes/-/blob/main/input/DE_RvsS_all.txt)